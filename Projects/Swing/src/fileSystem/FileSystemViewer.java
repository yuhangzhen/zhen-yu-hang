package fileSystem;

import javax.swing.*;

public class FileSystemViewer extends JFrame{
	JTree tree = new JTree();

	public static void main(String[] args) {
		FileSystemViewer win = new FileSystemViewer();
		win.setVisible(true);

	}
	public FileSystemViewer() {
		super();
		init();
	}
	private void init() {
		this.getContentPane().add(tree);
		tree.setModel(new FileSystemTreeModel());
		}
}
