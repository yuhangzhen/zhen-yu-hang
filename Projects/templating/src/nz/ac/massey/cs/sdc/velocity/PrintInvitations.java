package nz.ac.massey.cs.sdc.velocity;

import java.io.FileWriter;
import java.io.IOException;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class PrintInvitations {

	public static void main(String[] args) throws Exception {
		
		Student s = new Student("Tom","Smith","BSc",new String[]{"CompSci","Math"});
		
		VelocityContext context = new VelocityContext();

		context.put( "student", s);
		
		Template template = Velocity.getTemplate("letter.vm");
		
		FileWriter out = new FileWriter("letter1.txt");

		template.merge( context, out );
		
		out.close();
		
	}

}
